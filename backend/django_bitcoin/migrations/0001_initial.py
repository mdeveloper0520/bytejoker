# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-22 08:54
from __future__ import unicode_literals

import datetime
from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BitcoinAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=50, unique=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('active', models.BooleanField(default=False)),
                ('least_received', models.DecimalField(decimal_places=8, default=Decimal('0'), max_digits=16)),
                ('least_received_confirmed', models.DecimalField(decimal_places=8, default=Decimal('0'), max_digits=16)),
                ('label', models.CharField(blank=True, default=None, max_length=50, null=True)),
            ],
            options={
                'verbose_name_plural': 'Bitcoin addresses',
            },
        ),
        migrations.CreateModel(
            name='HistoricalPrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=16)),
                ('params', models.CharField(max_length=50)),
                ('currency', models.CharField(max_length=10)),
            ],
            options={
                'verbose_name': 'HistoricalPrice',
                'verbose_name_plural': 'HistoricalPrices',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=255)),
                ('address', models.CharField(max_length=50)),
                ('amount', models.DecimalField(decimal_places=8, default=Decimal('0.0'), max_digits=16)),
                ('amount_paid', models.DecimalField(decimal_places=8, default=Decimal('0.0'), max_digits=16)),
                ('active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField()),
                ('paid_at', models.DateTimeField(default=None, null=True)),
                ('withdrawn_total', models.DecimalField(decimal_places=8, default=Decimal('0.0'), max_digits=16)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('amount', models.DecimalField(decimal_places=8, default=Decimal('0.0'), max_digits=16)),
                ('address', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField()),
                ('label', models.CharField(blank=True, max_length=50)),
                ('transaction_counter', models.IntegerField(default=1)),
                ('last_balance', models.DecimalField(decimal_places=8, default=Decimal('0'), max_digits=16)),
            ],
        ),
        migrations.CreateModel(
            name='WalletTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now)),
                ('to_bitcoinaddress', models.CharField(blank=True, max_length=50)),
                ('amount', models.DecimalField(decimal_places=8, default=Decimal('0.0'), max_digits=16)),
                ('description', models.CharField(blank=True, max_length=100)),
                ('from_wallet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sent_transactions', to='django_bitcoin.Wallet')),
                ('to_wallet', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='received_transactions', to='django_bitcoin.Wallet')),
            ],
        ),
        migrations.AddField(
            model_name='wallet',
            name='transactions_with',
            field=models.ManyToManyField(through='django_bitcoin.WalletTransaction', to='django_bitcoin.Wallet'),
        ),
        migrations.AddField(
            model_name='payment',
            name='transactions',
            field=models.ManyToManyField(to='django_bitcoin.Transaction'),
        ),
        migrations.AddField(
            model_name='bitcoinaddress',
            name='wallet',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='addresses', to='django_bitcoin.Wallet'),
        ),
    ]
