from rest_framework import serializers
from accounts.models import User
from base.models import Transaction
from django.db.models import Min, Max
from django.core.exceptions import ValidationError

import json

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = (
                'activation_key', 'confirmed_email', 'date_joined',
                'is_active', 'is_staff', 'is_superuser',
                'last_login', 'password', 'email')

class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = '__all__'
    btc_address = serializers.CharField(max_length = 34)
    btc_amount = serializers.FloatField()
    btc_status = serializers.CharField(max_length = 100)
    btc_hash = serializers.CharField(max_length = 256)
    user = serializers.UUIDField(required = True)

class WithdrawSerializer(serializers.Serializer):
    address = serializers.CharField(max_length=34)
    amount = serializers.FloatField()
    label = serializers.CharField(max_length=100)

class DepositSerializer(serializers.Serializer):
    address = serializers.CharField(max_length=34)
    amount = serializers.FloatField()
    label = serializers.CharField(max_length=100)
    message = serializers.CharField(max_length=500)