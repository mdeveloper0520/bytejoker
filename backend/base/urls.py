from django.conf.urls import url, include

from base import views as base_views
from rest_framework.routers import SimpleRouter

router = SimpleRouter(trailing_slash=False)
router.register(r'users', base_views.UserViewSet)
router.register(r'transactions', base_views.TransactionViewSet)
router.register(r'withdraw', base_views.WithdrawalViewSet)
router.register(r'deposit', base_views.DepositViewSet)

urlpatterns = [
        url(r'^', include(router.urls)),
]
