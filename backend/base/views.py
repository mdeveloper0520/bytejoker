import os
import json
from django.conf import settings
from django.http import HttpResponse
from django.views.generic import View
from base.serializers import *
from accounts.models import User
from base.models import Transaction
from rest_framework import viewsets, mixins
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework import status

from django_bitcoin.models import *

from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
User = get_user_model()
import logging

class IndexView(View):
    def get(self, request):
        abspath = open(os.path.join(
            settings.BASE_DIR, '../frontend/build/index.html'), 'r')
        return HttpResponse(content=abspath.read())
class UserViewSet(
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, pk=None):
        if request.user and pk == 'current':
            return Response(UserSerializer(request.user).data)
        return super(UserViewSet, self).retrieve(request, pk)

class TransactionViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet):

    queryset  = Transaction.objects.all()
    serializer_class = TransactionSerializer

    def create(self,request):
        ts = self.serializer_class(data = request.data)

        if ts.is_valid():
            transaction = ts.save(user = self.request.user)
            transaction.save()

            return Response(ts.data, status = status.HTTP_200_OK)
        return Response(ts.errors, status = status.HTTP_400_BAD_REQUEST)

    def list(self,request):
        queryset = Transaction.objects.filter(user=self.request.user)
        serializer = TransactionSerializer(queryset, many=True,
                                    context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class WithdrawalViewSet(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated, )
    queryset = Wallet.objects.all()
    serializer_class = WithdrawSerializer

    def create(self, request):
        withdraw_data = self.serializer_class(data=request.data)
        if withdraw_data.is_valid():
            address = withdraw_data.validated_data.get('address')
            amount = withdraw_data.validated_data.get('amount')
            label = withdraw_data.validated_data.get('label')
            try:
                request.user.wallet.send_to_address(address, amount, label)
            except Exception as e:
                return Response({'errors': e.message}, status=status.HTTP_403_FORBIDDEN)

            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=withdraw_data.errors)


class DepositViewSet(viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = DepositSerializer
    queryset = Wallet.objects.all()

    def create(self, request):
        deposit_serializer = self.serializer_class(data=request.data)
        if deposit_serializer.is_valid():
            address = deposit_serializer.validated_data.get('address')
            amount = deposit_serializer.validated_data.get('amount')
            label = deposit_serializer.validated_data.get('label')
            message = deposit_serializer.validated_data.get('message')
            try:
                bitcoin_address = BitcoinAddress.objects.get(address=address)
            except BitcoinAddress.DoesNotExist as e:
                return Response({'error': 'Bitcoin Address doesnot exist'}, status=status.HTTP_403_FORBIDDEN)
            try:
                request.user.wallet.send_to_wallet(bitcoin_address.wallet, amount, message)
            except Exception as e:
                return Response({'error' :e.message}, status=status.HTTP_403_FORBIDDEN)


        return Response(status=status.HTTP_200_OK)