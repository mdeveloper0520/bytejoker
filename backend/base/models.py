import uuid
from django.db import models
from django.contrib.postgres.fields import JSONField
from accounts.models import User


class Transaction(models.Model):
    uuid = models.UUIDField(
        unique = True, default= uuid.uuid4, editable = False, primary_key = True)
    btc_address = models.CharField(max_length = 34)
    btc_amount = models.FloatField()
    btc_status = models.CharField(max_length = 100)
    btc_hash = models.CharField(max_length = 256)
    user = models.ForeignKey(User, on_delete = models.CASCADE)

