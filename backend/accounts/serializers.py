from rest_framework import serializers
from accounts.models import User
from lib.utils import validate_email as email_is_valid
import logging
import string
import random
logger = logging.getLogger('django')

class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('uuid', 'email', 'fb_id', 'fb_name',
                  )

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        return user

    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """

        if not email_is_valid(value):
            raise serializers.ValidationError(
                    'Please use a different email address provider.')

        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError(
                'Email already in use, please use a different email address.')

        return value
class UserFBLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('uuid', 'email', 'fb_id', 'fb_name',
                  )

    def create(self, validated_data):
        if not (validated_data.has_key('alias')):
            validated_data['alias'] = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        user = User.objects.create(**validated_data)
        return user
    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """

        if not email_is_valid(value):
            raise serializers.ValidationError(
                    'Please use a different email address provider.')

        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError(
                'Email already in use, please use a different email address.')

        return value
