from rest_framework_jwt.settings import api_settings
from datetime import datetime
from calendar import timegm
from accounts.models import User
from django.core.exceptions import ValidationError, ObjectDoesNotExist

def jwt_payload_handler(user):
    payload = {
        'fb_id': user.fb_id,
        'fb_name': user.fb_name,
        # 'user_isadmin': user.is_admin,
        'email': user.email,
        'alias': user.alias,
        'balance': user.balance,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    return payload


def jwt_payload_get_username_handler(data):
    try:
        u = User.objects.get(email=data['email'])
        return u.email
    except ObjectDoesNotExist:
        raise ValidationError('This user does not exist.')
