function createHandler(component, key, path) {
  return (e) => {
    const el = e.target;
    const value = el.type === 'checkbox' ? el.checked : el.value;
    component.setState({
      [key]: path ? component.state[key].setIn(path, value) : value,
    });
  };
}

module.exports = function linkState(component, key, path) {
  const cache = component.__linkStateHandlers ||
    (component.__linkStateHandlers = {});
  const cacheKey = path ? `${key}:${path.toString()}` : key;

  return cache[cacheKey] ||
    (cache[cacheKey] = createHandler(component, key, path));
}
