import { createReducer } from '../utils';
import {
    AUTH_LOGIN_USER_REQUEST,
    AUTH_LOGIN_USER_SUCCESS,
    AUTH_LOGIN_USER_FAILURE,
    AUTH_LOGOUT_USER
} from '../constants';
import jwtDecode from 'jwt-decode';
import randomString from 'random-string';
const initialState = {
    token: null,
    fb_id: null,
    fb_name: null,
    email: null,
    balance: 0,
    alias: randomString({
        length: 10,
        numeric: true,
        letters: true,
        special: false,
      }).toUpperCase(),
    isAuthenticated: false,
    isAuthenticating: false,
    statusText: null
};

export default createReducer(initialState, {
    [AUTH_LOGIN_USER_REQUEST]: (state, payload) => {
        return Object.assign({}, state, {
            isAuthenticating: true,
            statusText: null
        });
    },
    [AUTH_LOGIN_USER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            isAuthenticating: false,
            isAuthenticated: true,
            token: payload.token,
            fb_id: jwtDecode(payload.token).fb_id,
            fb_name: jwtDecode(payload.token).fb_name,
            email: jwtDecode(payload.token).email,
            alias: jwtDecode(payload.token).alias,
            statusText: 'You have been successfully logged in.'
        });
    },
    [AUTH_LOGIN_USER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            isAuthenticating: false,
            isAuthenticated: false,
            token: null,
            userName: null,
            statusText: `Authentication Error: ${payload.status} ${payload.statusText}`
        });
    },
    [AUTH_LOGOUT_USER]: (state, payload) => {
        return Object.assign({}, state, {
            isAuthenticated: false,
            token: null,
            userName: null,
            statusText: 'You have been successfully logged out.'
        });
    }
});
