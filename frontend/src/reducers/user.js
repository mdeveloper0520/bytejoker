import { createReducer } from '../utils';
import {
  USER_GET_CURRENT_REQUEST,
  USER_GET_CURRENT_SUCCESS,
  USER_GET_CURRENT_FAILURE
} from '../constants';
import jwtDecode from 'jwt-decode';
import randomString from 'random-string';

const initialState = {
  isGettingCurrentUser: false,
  hasGottenCurrentUser: false,
  user: null,
  statusText: null
};

export default createReducer(initialState, {
  [USER_GET_CURRENT_REQUEST]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingCurrentUser: true,
          hasGottenCurrentUser: false,
          user: null,
          statusText: null
      });
  },
  [USER_GET_CURRENT_SUCCESS]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingCurrentUser: false,
          hasGottenCurrentUser: true,
          user: payload,
          statusText: null
      });
  },
  [USER_GET_CURRENT_FAILURE]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingCurrentUser: false,
          hasGottenCurrentUser: false,
          user: null,
          statusText: `${payload.statusText}`
      });
  },
});
