import { createReducer } from '../utils';
import {
  TRANSACTION_DEPOSIT_REQUEST,
  TRANSACTION_DEPOSIT_SUCCESS,
  TRANSACTION_DEPOSIT_FAILURE,
  TRANSACTION_WITHDRAW_REQUEST,
  TRANSACTION_WITHDRAW_SUCCESS,
  TRANSACTION_WITHDRAW_FAILURE,
  TRANSACTION_FAUCET_REQUEST,
  TRANSACTION_FAUCET_SUCCESS,
  TRANSACTION_FAUCET_FAILURE,
  TRANSACTION_CREATE_REQUEST,
  TRANSACTION_CREATE_SUCCESS,
  TRANSACTION_CREATE_FAILURE,
  TRANSACTION_LIST_REQUEST,
  TRANSACTION_LIST_SUCCESS,
  TRANSACTION_LIST_FAILURE,
  TRANSACTION_GET_REQUEST,
  TRANSACTION_GET_SUCCESS,
  TRANSACTION_GET_FAILURE
} from '../constants';
import jwtDecode from 'jwt-decode';

const initialState = {
    transactionList: null,
    transaction: null,
    transactionID: null,
    isDepositing: false,
    hasDeposited: false,
    isWithdrawing: false,
    hasWithdrawn: false,
    isProcessingFaucet: false,
    hasFinishedFaucet: false,
    isGettingTransaction: false,
    hasGottenTransaction: false,
    isGettingList: false,
    hasGottenList: false,
    isCreating: false,
    isCreated: false,
    statusText: null
};

export default createReducer(initialState, {
  [TRANSACTION_CREATE_REQUEST]: (state, payload) => {
      return Object.assign({}, state, {
          transactionID: null,
          isCreating: true,
          isCreated: false,
          statusText: null
      });
  },
  [TRANSACTION_CREATE_SUCCESS]: (state, payload) => {
      return Object.assign({}, state, {
          transactionID: payload.uuid,
          isCreating: false,
          isCreated: true,
          statusText: 'You have successfully created a transaction.'
      });
  },
  [TRANSACTION_CREATE_FAILURE]: (state, payload) => {
      return Object.assign({}, state, {
          transactionID: null,
          isCreating: false,
          isCreated: false,
          statusText: `Transaction creation error: ${payload.statusText}`
      });
  },
  [TRANSACTION_LIST_REQUEST]: (state, payload) => {
      return Object.assign({}, state, {
          transactionList: null,
          isGettingList: true,
          hasGottenList: false,
          statusText: null
      });
  },
  [TRANSACTION_LIST_SUCCESS]: (state, payload) => {
      return Object.assign({}, state, {
          transactionList: payload,
          isGettingList: false,
          hasGottenList: true,
          statusText: 'List of transactions returned'
      });
  },
  [TRANSACTION_LIST_FAILURE]: (state, payload) => {
      return Object.assign({}, state, {
          transactionList: null,
          isGettingList: false,
          hasGottenList: false,
          statusText: `Transaction list error: ${payload.statusText}`
      });
  },
  [TRANSACTION_GET_REQUEST]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingTransaction: true,
          hasGottenTransaction: false,
          transaction: null,
          statusText: null
      });
  },
  [TRANSACTION_GET_SUCCESS]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingTransaction: false,
          hasGottenTransaction: true,
          transaction: payload,
          statusText: 'Transaction returned'
      });
  },
  [TRANSACTION_GET_FAILURE]: (state, payload) => {
      return Object.assign({}, state, {
          isGettingTransaction: false,
          hasGottenTransaction: false,
          transaction: null,
          statusText: `Transaction error: ${payload.statusText}`
      });
  },


});
