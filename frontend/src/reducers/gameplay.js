import { createReducer } from '../utils';

const room_info = {
  room_pot: 0,
  room_players: 0,
  time_remaining: 0,
  time_started : 0,
  room_id: 0
}
const initialState = {
  game_mode : 0,
  game_info: {
    'hour':room_info,
    'minute':room_info,
    'day':room_info
  }
};

export default createReducer(initialState, {

});
