import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import authReducer from './auth';
import userReducer from './user';
import modalReducer from './modal';
import transactionReducer from './transaction';
import gameplayReducer from './gameplay';
/*
import dataReducer from './data';
import accountReducer from './account';
import buildingReducer from './building';
import neighborhoodReducer from './neighborhood';
import unitReducer from './unit';
import reviewReducer from './review';
*/
export default combineReducers({
    auth: authReducer,
    user: userReducer,
    modal: modalReducer,
    transaction: transactionReducer,
    gameplay: gameplayReducer,
    routing: routerReducer
});
