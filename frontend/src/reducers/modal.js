import { createReducer } from '../utils';
import {
  MODAL_WITHDRAW_OPEN,
  MODAL_WITHDRAW_CLOSE,
  MODAL_DEPOSIT_OPEN,
  MODAL_DEPOSIT_CLOSE,
  MODAL_FAUCET_OPEN,
  MODAL_FAUCET_CLOSE,
} from '../constants';

const initialState = {
  isWithdrawModalOpen: false,
  isFaucetModalOpen: false,
  isDepositModalOpen: false,

};

export default createReducer(initialState, {
  [MODAL_WITHDRAW_OPEN]: (state, payload) => {
      return Object.assign({}, state, {
        isWithdrawModalOpen : true
      });
  },
  [MODAL_WITHDRAW_CLOSE]: (state, payload) => {
      return Object.assign({}, state, {
        isWithdrawModalOpen : false
      });
  },
  [MODAL_DEPOSIT_OPEN]: (state, payload) => {
      return Object.assign({}, state, {
        isDepositModalOpen : true
      });
  },
  [MODAL_DEPOSIT_CLOSE]: (state, payload) => {
      return Object.assign({}, state, {
        isDepositModalOpen : false
      });
  },
  [MODAL_FAUCET_OPEN]: (state, payload) => {
      return Object.assign({}, state, {
        isFaucetModalOpen : true
      });
  },
  [MODAL_FAUCET_CLOSE]: (state, payload) => {
      return Object.assign({}, state, {
        isFaucetModalOpen : false
      });
  },

});
