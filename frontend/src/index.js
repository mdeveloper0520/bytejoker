import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import ReactDOM from 'react-dom';
import App from './components/App';
import DevTools from './containers/Root/DevTools';

import configureStore from './store/configureStore';
import { BrowserRouter } from 'react-router-dom';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { authLoginUserSuccess } from './actions/auth';

const store = configureStore(window.INITIAL_STATE, browserHistory);
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
      <div>
          <BrowserRouter history={history}>
              <App />
          </BrowserRouter>
          <DevTools/>
      </div>
  </Provider>,
  document.getElementById('root')
);
const token = localStorage.getItem('token');
if (token !== null) {
    store.dispatch(authLoginUserSuccess(token));
}
