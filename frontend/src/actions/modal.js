import {
  MODAL_WITHDRAW_OPEN,
  MODAL_WITHDRAW_CLOSE,
  MODAL_DEPOSIT_OPEN,
  MODAL_DEPOSIT_CLOSE,
  MODAL_FAUCET_OPEN,
  MODAL_FAUCET_CLOSE,
} from '../constants';

export function openWithdrawModal() {
    return {
        type: MODAL_WITHDRAW_OPEN,
        payload: {
        }
    };
}

export function closeWithdrawModal() {
    return {
        type: MODAL_WITHDRAW_CLOSE,
        payload: {
        }
    };
}

export function openFaucetModal() {
    return {
        type: MODAL_FAUCET_OPEN,
        payload: {
        }
    };
}

export function closeFaucetModal() {
    return {
        type: MODAL_FAUCET_CLOSE,
        payload: {
        }
    };
}

export function openDepositModal() {
    return {
        type: MODAL_DEPOSIT_OPEN,
        payload: {
        }
    };
}

export function closeDepositModal() {
    return {
        type: MODAL_DEPOSIT_CLOSE,
        payload: {
        }
    };
}
