import fetch from 'isomorphic-fetch';
import { SERVER_URL } from '../utils/config';
import { checkHttpStatus, parseJSON } from '../utils';
import {
    USER_GET_CURRENT_REQUEST, USER_GET_CURRENT_SUCCESS, USER_GET_CURRENT_FAILURE, USER_GET_CURRENT_RESET
} from '../constants';

export function currentUserGetSuccess(user) {
    return {
        type: USER_GET_CURRENT_SUCCESS,
        payload: user
    };
}

export function currentUserGetFailure(error) {
    return {
        type: USER_GET_CURRENT_FAILURE,
        payload: {
            status: error.response.status,
            statusText: error.response.statusText
        }
    };
}

export function currentUserGetRequest() {
    return {
        type: USER_GET_CURRENT_REQUEST
    };
}

export function currentUserGetReset() {
    return {
        type: USER_GET_CURRENT_RESET
    };
}

export function resetCurrentUser() {
    return (dispatch) => {
        dispatch(currentUserGetReset());
    }
}

export function getCurrentUser(token) {
    return (dispatch) => {
        dispatch(currentUserGetRequest());
        return fetch(`${SERVER_URL}/api/v1/base/users/current`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: `JWT ${token}`
            },
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then(response => {
                try {
                    dispatch(currentUserGetSuccess(response));
                } catch (e) {
                    dispatch(currentUserGetFailure({
                        response: {
                            status: 403,
                            statusText: e
                        }
                    }));
                }
            })
            .catch(error => {
                dispatch(currentUserGetFailure(error));
            });
    };
}
