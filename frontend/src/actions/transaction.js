import fetch from 'isomorphic-fetch';
import { push } from 'react-router-redux';
import jwtDecode from 'jwt-decode';
import { SERVER_URL } from '../utils/config';
import { checkHttpStatus, parseJSON } from '../utils';
import {
  TRANSACTION_DEPOSIT_REQUEST,
  TRANSACTION_DEPOSIT_SUCCESS,
  TRANSACTION_DEPOSIT_FAILURE,
  TRANSACTION_WITHDRAW_REQUEST,
  TRANSACTION_WITHDRAW_SUCCESS,
  TRANSACTION_WITHDRAW_FAILURE,
  TRANSACTION_FAUCET_REQUEST,
  TRANSACTION_FAUCET_SUCCESS,
  TRANSACTION_FAUCET_FAILURE,
  TRANSACTION_CREATE_REQUEST,
  TRANSACTION_CREATE_SUCCESS,
  TRANSACTION_CREATE_FAILURE,
  TRANSACTION_LIST_REQUEST,
  TRANSACTION_LIST_SUCCESS,
  TRANSACTION_LIST_FAILURE,
  TRANSACTION_GET_REQUEST,
  TRANSACTION_GET_SUCCESS,
  TRANSACTION_GET_FAILURE
} from '../constants';

export function transactionCreateSuccess(transactionID) {
    return {
        type: TRANSACTION_CREATE_SUCCESS,
        payload: {
            transactionID: transactionID
        }
    };
}

export function transactionCreateFailure(error) {
    return {
        type: TRANSACTION_CREATE_FAILURE,
        payload: {
            status: error.response.status,
            statusText: error.response.statusText
        }
    };
}

export function transactionCreateRequest() {
    return {
        type: TRANSACTION_CREATE_REQUEST
    };
}

export function createTransaction(token, btc_address, btc_amount, btc_status, btc_hash, redirect) {
    return (dispatch) => {
        dispatch(transactionCreateRequest());

        // We'll build a form data object because we're packing files with it too
        var data = new FormData()
        data.append('btc_address', btc_address);
        data.append('btc_amount', btc_amount);
        data.append('btc_status', btc_status);
        data.append('btc_hash', btc_hash);

        return fetch(`${SERVER_URL}/api/v1/base/transactions`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                Authorization: `JWT ${token}`
            },
            body: data
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then(response => {
                try {
                    dispatch(transactionCreateSuccess(response));
                    console.log(response);
                } catch (e) {
                    dispatch(transactionCreateFailure({
                        response: {
                            status: 403,
                            statusText: 'Error creating transaction.'
                        }
                    }));
                }
            })
            .catch(error => {
                dispatch(transactionCreateFailure(error));
            });
    };
}

export function transactionListSuccess(transactionList) {
    return {
        type: TRANSACTION_LIST_SUCCESS,
        payload: transactionList
    };
}

export function transactionListFailure(error) {
    return {
        type: TRANSACTION_LIST_FAILURE,
        payload: {
            status: error.response.status,
            statusText: error.response.statusText
        }
    };
}

export function transactionListRequest() {
    return {
        type: TRANSACTION_LIST_REQUEST
    };
}

export function listTransactions(token) {

    return (dispatch) => {
        dispatch(transactionListRequest());
        return fetch(`${SERVER_URL}/api/v1/base/transactions`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: `JWT ${token}`
            },
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then(response => {
                try {
                    dispatch(transactionListSuccess(response));
                } catch (e) {
                    dispatch(transactionListFailure({
                        response: {
                            status: 403,
                            statusText: e
                        }
                    }));
                }
            })
            .catch(error => {
                dispatch(transactionListFailure(error));
            });
    };
}

export function transactionGetSuccess(transactionGet) {
    return {
        type: TRANSACTION_GET_SUCCESS,
        payload: transactionGet
    };
}

export function transactionGetFailure(error) {
    return {
        type: TRANSACTION_GET_FAILURE,
        payload: {
            status: error.response.status,
            statusText: error.response.statusText
        }
    };
}

export function transactionGetRequest() {
    return {
        type: TRANSACTION_GET_REQUEST
    };
}

export function getTransaction(token, transactionID) {
    return (dispatch) => {
        dispatch(transactionGetRequest());

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }

        if (token != null) {
            headers.Authorization = `JWT ${token}`
        }

        return fetch(`${SERVER_URL}/api/v1/base/transactions/${transactionID}`, {
            method: 'get',
            headers
        })
            .then(checkHttpStatus)
            .then(parseJSON)
            .then(response => {
                try {
                    dispatch(transactionGetSuccess(response));
                } catch (e) {
                    dispatch(transactionGetFailure({
                        response: {
                            status: 403,
                            statusText: e
                        }
                    }));
                }
            })
            .catch(error => {
                dispatch(transactionGetFailure(error));
            });
    };
}
