import React from "react";
import Header from "./Header/header";
import Footer from "./Footer/footer";

class About extends React.Component {
  render() {
    return (
      <div className="wrapper">

        <Header />
        <main className="main">
      		<div className="shell">
      			<section className="section-about">
      				<header className="section__head">
      					<h1 className="section__title">ABOUT</h1>

      					<h6 className="section__subtitle">BETTING GAME THAT TRANSFORMS MINDS</h6>
      				</header>
              <div className="section__body">
      					<div className="slider-fullpage">
      						<div className="slider__clip">
      							<div className="slider__slides js-slider-fullpage">
      								<div className="slider__slide">
      									<article className="article">
      										<h2 className="article__title visible-xs-block">STORY OF CONCEPTION</h2>

      										<div className="article__image">
      											<figure>
      												<img src="css/images/temp/article-image-1.png" alt="" />
      											</figure>

      											<div className="article__image-inner">
      												<p className="article__meta">
      													<small>GAMES PLAYED</small>

      													<span>73’000’433</span>
      												</p>

      												<p className="article__meta">
      													<small>AVERAGE INCOME</small>

      													<span>300’433</span>
      												</p>
      											</div>
      										</div>

      										<div className="article__content article__content--alt">
      											<h2 className="article__title hidden-xs">STORY OF CONCEPTION</h2>

      											<h6 className="article__subtitle">In his comic book appearances, the Joker is portrayed as a criminal mastermind ashtray beyond belief.</h6>

      											<blockquote>
      												Betting had never been more engagic and beyond bitcoin life-fullfilling expeience with transformations
      											</blockquote>

      											<div className="cols">
      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>

      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>
      											</div>
      										</div>
      									</article>
      								</div>

      								<div className="slider__slide">
      									<article className="article">
      										<h2 className="article__title visible-xs-block">BETTING DYNAMICS</h2>

      										<div className="article__image article__image--with-border">
      											<figure>
      												<img src="css/images/temp/article-image-2.png" alt="" />
      											</figure>

      											<div className="article__image-inner">
      												<p className="article__meta article__meta--orange">
      													<small>AVERAGE INCOME</small>

      													<span>300’433</span>
      												</p>

      												<p className="article__meta article__meta--orange">
      													<small>AVERAGE INCOME</small>

      													<span>300’433</span>
      												</p>
      											</div>
      										</div>

      										<div className="article__content">
      											<h2 className="article__title hidden-xs">BETTING DYNAMICS</h2>

      											<h6 className="article__subtitle">In his comic book appearances, the Joker is portrayed as a criminal mastermind ashtray.</h6>

      											<div className="cols">
      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>

      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>
      											</div>
      										</div>
      									</article>
      								</div>

      								<div className="slider__slide">
      									<article className="article">
      										<h2 className="article__title visible-xs-block">OUR MISSION AND FUTURE</h2>

      										<div className="article__image">
      											<figure>
      												<img src="css/images/temp/article-image-3.png" alt="" />
      											</figure>

      											<div className="article__image-inner">
      												<p className="article__meta article__meta--pink">
      													<small>GAMES PLAYED</small>

      													<span>73’000’433</span>
      												</p>

      												<p className="article__meta article__meta--pink">
      													<small>AVERAGE INCOME</small>

      													<span>300’433</span>
      												</p>
      											</div>
      										</div>

      										<div className="article__content article__content--alt">
      											<h2 className="article__title hidden-xs">OUR MISSION AND FUTURE</h2>

      											<h6 className="article__subtitle">In his comic book appearances, the Joker is portrayed as a criminal mastermind ashtray beyond belief.</h6>

      											<blockquote>
      												Betting had never been more engagic and beyond bitcoin life-fullfilling expeience with transformations
      											</blockquote>

      											<div className="cols">
      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>

      												<div className="col col--1of2">
      													<p>Introduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code AuthorityIntroduced as a psychopath with a warped, sadistic sense of humor, the character became a goofy prankster in the late 1950s in response to regulation by the Comics Code Authority</p>
      												</div>
      											</div>
      										</div>
      									</article>
      								</div>
      							</div>
      						</div>
      					</div>
      				</div>
            </section>
      		</div>
      	</main>

        <Footer />

      </div>
    );
  }
}

export default About;
