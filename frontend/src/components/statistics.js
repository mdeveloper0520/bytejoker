import React from "react";
import Header from "./Header/header";
import Footer from "./Footer/footer";
import Select2 from 'react-select2-wrapper';
import $ from "jquery";

class Statistics extends React.Component {
  componentDidMount = () =>{
    //$('#chart-select-1');
  }
  render() {
    return (
      <div className="wrapper">

        <Header />
        <main className="main">
      		<section className="section-default">
      			<header className="section__head">
      				<div className="shell">
      					<h1 className="section__title">STATS</h1>

      					<h6 className="section__subtitle">ONE BITCOIN IS HAPPINESS, THOUSAND BITCOINS IS STATISTIC</h6>
      				</div>
      			</header>

      			<div className="section__body mCustomScrollbar">
      				<div className="shell">
      					<div className="chart js-chart">
      						<header className="chart__head">
      							<div className="chart__head-figure">
      								<i className="ico-ornament"></i>
      							</div>

      							<div className="chart__head-inner">
      								<p className="chart__meta">
      									<small>GAMES PLAYED</small>

      									<span>73’000’433</span>
      								</p>

      								<div className="chart__select">
                        <Select2
                          name="chart-select-1"
                          id="chart-select-1"
                          className="js-chart-select"
                          data={[
                            {text:'Today',id:1},
                            {text:'Last 30 days',id:2},
                            {text:'Last 12 Months',id:3}
                          ]}
                          defaultValue={1}
                          options={
                            {
                              minimumResultsForSearch: Infinity
                            }
                          }
                        />
      								</div>
      							</div>
      						</header>

      						<div className="chart__body js-chart-holder">
      						</div>
      					</div>

      					<div className="chart js-chart">
      						<header className="chart__head">
      							<div className="chart__head-figure">
      								<i className="ico-ornament"></i>
      							</div>

      							<div className="chart__head-inner">
      								<p className="chart__meta">
      									<small>GAMES PLAYED</small>

      									<span>73’000’433</span>
      								</p>

      								<div className="chart__select">
      									<select name="chart-select-2" id="chart-select-2" className="js-chart-select">
      										<option value="json/data-today.json" selected>Today</option>
      										<option value="json/data-month.json">Last 30 days</option>
      										<option value="json/data-year.json">Last 12 Months</option>
      									</select>
      								</div>
      							</div>
      						</header>

      						<div className="chart__body js-chart-holder">
      						</div>
      					</div>hart -->

      					<div className="chart js-chart">
      						<header className="chart__head">
      							<div className="chart__head-figure">
      								<i className="ico-ornament"></i>
      							</div>

      							<div className="chart__head-inner">
      								<p className="chart__meta">
      									<small>GAMES PLAYED</small>

      									<span>73’000’433</span>
      								</p>

      								<div className="chart__select">
      									<select name="chart-select-3" id="chart-select-3" className="js-chart-select">
      										<option value="json/data-today.json" selected>Today</option>
      										<option value="json/data-month.json">Last 30 days</option>
      										<option value="json/data-year.json">Last 12 Months</option>
      									</select>
      								</div>
      							</div>
      						</header>

      						<div className="chart__body js-chart-holder">
      						</div>
      					</div>
      				</div>
      			</div>
      		</section>

      	</main>
        <Footer />

      </div>
    );
  }
}

export default Statistics;
