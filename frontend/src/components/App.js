import React from 'react';
import Home from "./home";
import About from "./about";
import Support from "./support";
import Affiliate from "./affiliate";
import Statistics from "./statistics";
import { Route } from 'react-router-dom';

class App extends React.Component {
    render() {
        return(
            <div>
              <Route exact path="/" component={Home}/>
              <Route path="/about" component={About}/>
              <Route path="/affiliate" component={Affiliate}/>
              <Route path="/statistics" component={Statistics}/>
              <Route path="/support" component={Support}/>
            </div>
        );
    }
}

export default App;
