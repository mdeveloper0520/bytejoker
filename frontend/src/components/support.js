import React from "react";
import Header from "./Header/header";
import Footer from "./Footer/footer";

class Support extends React.Component {
  render() {
    return (
      <div className="wrapper">

        <Header />

        <Footer />

      </div>
    );
  }
}

export default Support;
