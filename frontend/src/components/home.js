import React from "react";
import Header from "./Header/header";
import Main from "./Main/main";
import Footer from "./Footer/footer";
import PropTypes from 'prop-types';

class Home extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      email: '',
      password: ''
    }
  }
  render() {
    return (
      <div className="wrapper">

        <Header />

        <Main />

        <Footer />

      </div>
    );
  }
}

export default Home;
