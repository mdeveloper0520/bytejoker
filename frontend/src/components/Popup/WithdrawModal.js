import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../../actions/auth';
import * as userActionCreators from '../../actions/user';
import * as modalActionCreators from '../../actions/modal';

class WithdrawModal extends React.Component{
  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      token: PropTypes.string,
      actions: PropTypes.object.isRequired,
      userActions: PropTypes.object.isRequired,
      modalActions: PropTypes.object.isRequired,
      isWithdrawModalOpen: PropTypes.bool.isRequired
  };

  handleClose = (e) => {
    this.props.modalActions.closeWithdrawModal();
    e.preventDefault();
  }

  render(){
    return(
      <ReactModal
        className="popup popup--withdraw"
        isOpen={this.props.isWithdrawModalOpen}
        style={ {overlay: {zIndex: 10}}}
        >
        <div className="popup__inner">
          <a href="#" onClick={this.handleClose} className="js-popup-close popup__btn">
            <i className="ico-close"></i>
          </a>

          <div className="popup__ornaments">
            <span className="popup__ornament"></span>
            <span className="popup__ornament-top-right"></span>
            <span className="popup__ornament-bottom-right"></span>
            <span className="popup__ornament-bottom-left"></span>
          </div>

          <header className="popup__head">
            <h3 className="popup__title">WITHDRAW</h3>
          </header>

          <div className="popup__body">
            <div className="form-simple">
              <form action="?" method="post">
                <h4 className="form__title">
                  <span>YOUR ADDRESS</span>
                </h4>

                <div className="form__row">

                  <div className="form__controls">
                    <input type="text" className="field field--transparent" name="field-withdrawal-address" id="field-withdrawal-address" value="" placeholder="Enter your withdrawal address" />
                  </div>

                  <button type="submit" className="btn btn--ico form__btn">
                    <i className="ico-confirm"></i>

                    <span>CONFIRM</span>
                  </button>
                </div>
              </form>
            </div>

            <div className="form-simple">
              <form action="?" method="post">
                <h4 className="form__title">
                  <span>AMOUNT ( EUR)</span>
                </h4>

                <div className="form__row form__row--dark">

                  <div className="form__controls">
                    <input type="text" className="field field--alt" name="field-amount" id="field-amount" value="" placeholder="0" />
                  </div>

                  <button type="submit" className="btn form__btn form__btn--yellow">WITHDRAW</button>

                  <div className="form__message">
                    <p>Available: <span>0.12 EUR</span></p>

                    <i className="ico-lock"></i>
                  </div>
                </div>
              </form>
            </div>

            <p>A 0.48 EUR transaction fee will be deducted from your withdrawal amount. Minimum withdrawal amount is 2.39 EUR. Withdrawals will be completed when your deposit has reached two confirmations.</p>

            <p>In case of larger withdrawals, your withdrawal may be marked for manual processing. This is very rare, and manual withdrawals are processed on business days from 10:00 to 18:00 GMT.</p>
          </div>
        </div>
      </ReactModal>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        isWithdrawModalOpen: state.modal.isWithdrawModalOpen
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        actions: bindActionCreators(actionCreators, dispatch),
        userActions: bindActionCreators(userActionCreators, dispatch),
        modalActions: bindActionCreators(modalActionCreators, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawModal);
