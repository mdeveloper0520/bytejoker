import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../../actions/auth';
import * as userActionCreators from '../../actions/user';
import * as modalActionCreators from '../../actions/modal';

class FaucetModal extends React.Component{
  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      token: PropTypes.string,
      actions: PropTypes.object.isRequired,
      userActions: PropTypes.object.isRequired,
      modalActions: PropTypes.object.isRequired,
      isFaucetModalOpen: PropTypes.bool.isRequired
  };

  handleClose = (e) => {
    this.props.modalActions.closeFaucetModal();
    e.preventDefault();
  }

  render(){
    return(
      <ReactModal
        className="popup popup--faucet"
        isOpen={this.props.isFaucetModalOpen}
        style={ {overlay: {zIndex: 10}}}
        >
          <div className="popup__inner">
      			<a href="#" onClick={this.handleClose} className="js-popup-close popup__btn">
      				<i className="ico-close"></i>
      			</a>

      			<div className="popup__ornaments">
      				<span className="popup__ornament"></span>
      				<span className="popup__ornament-top-right"></span>
      				<span className="popup__ornament-bottom-right"></span>
      				<span className="popup__ornament-bottom-left"></span>
      			</div>

      			<header className="popup__head">
      				<h3 className="popup__title">BITCOIN FAUCET</h3>
      			</header>

      			<div className="popup__body">
      				<div className="faucet-tool">
      					<p className="faucet-tool__message">FREE</p>

      					<div className="faucet-tool__inner">
      						<span>0.01 EUR</span>

      						<small>Remaining: <span>300</span></small>
      					</div>

      					<a href="#" className="btn btn--light-green faucet-tool__btn">CLAIM </a>
      				</div>

      				<p>Faucet available only if your balance less than <span>0.01 EUR</span>.</p>

      				<div className="captcha">
      					<img src="css/images/temp/captcha.png" alt="" />
      				</div>

      				<p>Reach <strong>"Hero"</strong> level to upgrade your faucet to 0.02 EUR every 10 minutes.</p>

      				<a href="#" className="btn btn--ico">
      					<i className="ico-bell"></i>

      					<span>HOW TO PLAY THO?</span>
      				</a>
      			</div>
      		</div>
      </ReactModal>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        isFaucetModalOpen: state.modal.isFaucetModalOpen
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        actions: bindActionCreators(actionCreators, dispatch),
        userActions: bindActionCreators(userActionCreators, dispatch),
        modalActions: bindActionCreators(modalActionCreators, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FaucetModal);
