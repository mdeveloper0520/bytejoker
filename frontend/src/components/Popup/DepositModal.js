import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../../actions/auth';
import * as userActionCreators from '../../actions/user';
import * as modalActionCreators from '../../actions/modal';
import Select2 from 'react-select2-wrapper';
import linkState from '../../utils/linkState';
import { Map, List } from 'immutable';
import { TRANSACTION_AMOUNTS } from '../../constants/index.js';

class DepositModal extends React.Component{
  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      token: PropTypes.string,
      actions: PropTypes.object.isRequired,
      userActions: PropTypes.object.isRequired,
      modalActions: PropTypes.object.isRequired,
      isDepositModalOpen: PropTypes.bool.isRequired
  };

  constructor(props){
    super(props);
    this.state = {
      btc_info:Map({
        btc_amount : 150,
        btc_address: null,
    })
    }
    this.handleClose = this.handleClose.bind(this);
  }

  changeAmount = (e,option) => {
    var btc_amount = this.state.btc_info.get('btc_amount');
    if(option == '+'){
      for(var i = 0; i < TRANSACTION_AMOUNTS.length; i ++)
      {
        if(btc_amount < TRANSACTION_AMOUNTS[i])
        {
          this.setState({btc_info:this.state.btc_info.set('btc_amount',TRANSACTION_AMOUNTS[i])});
          return;
        }
      }
    }
    if(option == '-'){
      for(var i = TRANSACTION_AMOUNTS.length - 1 ; i >=0 ; i --)
      {
        if(btc_amount > TRANSACTION_AMOUNTS[i])
        {
          this.setState({btc_info:this.state.btc_info.set('btc_amount',TRANSACTION_AMOUNTS[i])});
          return;
        }
      }
    }
  }

  handleClose = (e) => {
    this.props.modalActions.closeDepositModal();
    e.preventDefault();
  }

  render(){
    return(
      <ReactModal
        className="popup popup--deposit"
        isOpen={this.props.isDepositModalOpen}
        style={ {overlay: {zIndex: 10}}}
        >
          <div className="popup__inner">
      			<a href="#" onClick={this.handleClose} className="js-popup-close popup__btn">
      				<i className="ico-close"></i>
      			</a>

      			<div className="popup__ornaments">
      				<span className="popup__ornament"></span>
      				<span className="popup__ornament-top-right"></span>
      				<span className="popup__ornament-bottom-right"></span>
      				<span className="popup__ornament-bottom-left"></span>
      			</div>

      			<header className="popup__head">
      				<h3 className="popup__title">DEPOSIT</h3>

      				<div className="counter-simple">
                <button
                  className="counter__btn"
                  onClick={(e) => this.changeAmount(e,'-')}
                  >
                    -
                </button>

      					<div className="counter__inner">
      						<input
                    type="text"
                    className="field field--transparent counter__field"
                    value = {this.state.btc_info.get('btc_amount')}
                    onChange={linkState(this,'btc_info',['btc_amount'])}
                    />

                  <Select2
                    name="field-type-currency-2"
                    id="field-type-currency-2"
                    className="select-small"
                    data={[
                      {text:'€ EUR',id:1},
                      {text:'$ USD',id:2}
                    ]}
                    defaultValue={1}
                    options={
                      {
                        minimumResultsForSearch: Infinity
                      }
                    }
                  />
      					</div>

                <button
                  className="counter__btn"
                  onClick = {(e) => this.changeAmount(e,'+')}
                  >
                    +
                </button>
      				</div>

      				<p>
      					Enter deposit amount or send <strong>any amount</strong> to the following address. <br />Minimum deposit amount is <strong>2.29 EUR</strong>.

      					<small>1 ɃTC = 1,150.91 EUR</small>
      				</p>

      				<div className="qr-code">
      					<img src="css/images/temp/qr-code.png" alt="" />
      				</div>
      			</header>

      			<div className="popup__body">
      				<div className="form-default">
      					<form action="?" method="post">
      						<h3 className="form__title">You personal address is</h3>

      						<div className="form__row">

      							<div className="form__controls">
      								<input type="text" className="field field--transparent" name="field-address" id="field-address" value="39egRbuikjcUd6fvr5CB5ciKrKXc7QJ4uq" />
      							</div>

      							<input type="submit" value="PAY" className="btn btn--light-blue form__btn" />
      						</div>

      						<div className="form__divider">
      							<span>Or</span>
      						</div>

      						<div className="form__row form__row--dark">

      							<div className="form__controls">
      								<input type="text" className="field field--alt" name="field-code" id="field-code" value="" placeholder="Redeem your code here" />
      							</div>

      							<input type="submit" value="ADD TO BALANCE" className="btn btn--light-blue form__btn" />
      						</div>
      					</form>
      				</div>
      			</div>
      		</div>
        </ReactModal>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        isDepositModalOpen: state.modal.isDepositModalOpen
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        actions: bindActionCreators(actionCreators, dispatch),
        userActions: bindActionCreators(userActionCreators, dispatch),
        modalActions: bindActionCreators(modalActionCreators, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DepositModal);
