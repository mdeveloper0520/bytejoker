import React from "react";
import HeaderInner from "./header_inner";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Header extends React.Component {

  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      token: PropTypes.string,
      isAuthenticated: PropTypes.bool.isRequired,
      isAuthenticating: PropTypes.bool.isRequired,
      statusText: PropTypes.string,
      fb_id: PropTypes.string,
      fb_name: PropTypes.string,
      email: PropTypes.string,
      alias: PropTypes.string,
      actions: PropTypes.object.isRequired,

  };

  constructor(props){
    super(props);
  }

  render() {
    return (
      <header className="header">
        <div className="shell">
          <Link to="/" className="btn-menu js-btn-mobile-menu visible-xs-block">
            <span />
            <span />
            <span />
          </Link>

          <Link to="/" className="logo hidden-xs">
            <i className="ico-logo" />
            <span>
              <i className="ico-byte">Byte</i>
              {this.props.token}
              <i className="ico-joker">Joker</i>
            </span>
          </Link>

          <HeaderInner />

        </div>
      </header>
    );
  }
}
const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        isAuthenticating: state.auth.isAuthenticating,
        statusText: state.auth.statusText,
        fb_id: state.auth.fb_id,
        fb_name: state.auth.fb_name,
        email: state.auth.email,
        alias: state.auth.alias,
        token: state.auth.token

    };
};
export default connect(mapStateToProps)(Header);

//export default Header;
