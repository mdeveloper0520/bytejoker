import React from "react";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ToggleDisplay from 'react-toggle-display';
import * as actionCreators from '../../actions/auth';
import * as userActionCreators from '../../actions/user';
import * as modalActionCreators from '../../actions/modal';
import * as transactionActionCreators from '../../actions/transaction';

import FacebookLogin from 'react-facebook-login';

class HeaderInner extends React.Component {
  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      token: PropTypes.string,
      isAuthenticated: PropTypes.bool.isRequired,
      isAuthenticating: PropTypes.bool.isRequired,
      statusText: PropTypes.string,
      fb_id: PropTypes.string,
      fb_name: PropTypes.string,
      email: PropTypes.string,
      alias: PropTypes.string,
      actions: PropTypes.object.isRequired,
      userActions: PropTypes.object.isRequired,
      modalActions: PropTypes.object.isRequired,
      transactionActions: PropTypes.object.isRequired,
  };

  constructor(props){
    super(props);
    this.state = { show: false};
  }

  handleLoginToggle = (e) => {
    this.setState({
      show: !this.state.show
    });
    e.preventDefault();
  }

  responseFacebook = (response) => {
    this.props.actions.authFBLoginUser(response.email,response.name,response.id);
  }

  componentDidUpdate(prevProps,prevState){
    if(this.props.token == prevProps.token )
      return;
    this.props.userActions.getCurrentUser(this.props.token);
  }

  openDepositModal = () => {
    this.props.modalActions.openDepositModal();
  }

  openWithdrawModal = () => {
    this.props.modalActions.openWithdrawModal();
    this.props.transactionActions.listTransactions(this.props.token);
  }

  openFaucetModal = () => {
    this.props.modalActions.openFaucetModal();
  }

  render() {
    return (
      <div className="header__inner">
				<p>Welcome, <a href="#">{this.props.alias}</a></p>

				<nav className="nav-access">
					<ul>
						<li className="has-drodpown">
							<a onClick = { this.handleLoginToggle }>
								<i className="ico-perf" ></i>
							</a>

              {this.props.isAuthenticated ?(
                  <ToggleDisplay if={this.state.show} className="nav-access__dropdown">
                  <div className="nav-access__aside">
                    <small>BALANCE</small>
                    <span>{this.props.balance} COINS</span>
                  </div>
                  <div className="nav-access__actions">
                    <Link to="/" onClick={this.openDepositModal} className="btn btn--blue btn--small btn--block">
                      DEPOSIT
                    </Link>
                    <Link to="/" onClick={this.openWithdrawModal} className="btn btn--block">
                      WITHDRAW
                    </Link>
                    <Link to="/" onClick={this.openFaucetModal} className="btn btn--block">
                      FAUCET
                    </Link>
                  </div>
                </ToggleDisplay>

              ): (
                <ToggleDisplay if={this.state.show} className="nav-access__dropdown">
                  <FacebookLogin
                      appId="314604565670077"
                      autoLoad={true}
                      fields="name,email,picture"
                      icon="fa-facebook"
                      callback={this.responseFacebook} />
                    </ToggleDisplay>
                )
              }
						</li>
					</ul>
				</nav>
			</div>

    );
  }

}
const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        isAuthenticating: state.auth.isAuthenticating,
        statusText: state.auth.statusText,
        fb_id: state.auth.fb_id,
        fb_name: state.auth.fb_name,
        email: state.auth.email,
        alias: state.auth.alias,
        token: state.auth.token,
        balance: state.auth.balance,
        user: state.user.user,
        isGettingCurrentUser: state.user.isGettingCurrentUser,
        hasGottenCurrentUser: state.user.hasGottenCurrentUser
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        actions: bindActionCreators(actionCreators, dispatch),
        userActions: bindActionCreators(userActionCreators, dispatch),
        modalActions: bindActionCreators(modalActionCreators, dispatch),
        transactionActions: bindActionCreators(transactionActionCreators, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderInner);
//export default HeaderInner;
