import React from "react";
import Section from "./section";
import SectionSimple from "./section_simple";
import SectionTickets from "./section_tickets";
import WithdrawModal from "../Popup/WithdrawModal";
import DepositModal from "../Popup/DepositModal";
import FaucetModal from "../Popup/FaucetModal";

class Main extends React.Component {
  render() {
    return (
      <main className="main">
        <div className="shell">

          <Section />

          <SectionSimple />

          <SectionTickets />

        </div>
        <WithdrawModal />
        <DepositModal />
        <FaucetModal />
      </main>
    );
  }
}

export default Main;
