import React from "react";
import Tickets from "./tickets";
import Select2 from 'react-select2-wrapper';

class SectionHall extends React.Component {
  render() {
    return (
      <div>
        <h2 className="widget__title">HALL OF FAME</h2>

        <Select2
          name="rank-list-select"
          id="rank-list-select"
          className="widget__select"
          data={[
            {text:'Daily',id:1},
            {text:'Hourly',id:2},
            {text:'Monthly',id:3}
          ]}
          defaultValue={3}
          options={
            {
              minimumResultsForSearch: Infinity
            }
          }
        />

        <div className="table table--alt">
          <div className="table__head">
            <div className="table__row">
              <div className="table__cell">RANK</div>

              <div className="table__cell">Player</div>

              <div className="table__cell">WAGERED</div>

            </div>

          </div>

          <div className="table__body">
            <div className="table__row">
              <div className="table__cell">
                <i className="ico-ribbon-light">
                  <span>1</span>
                </i>
              </div>

              <div className="table__cell">7843iuf</div>

              <div className="table__cell table__cell--yellow">
                €3,469.21
              </div>

            </div>

            <div className="table__row">
              <div className="table__cell">
                <i className="ico-ribbon-light">
                  <span>2</span>
                </i>
              </div>

              <div className="table__cell">7843iuf</div>

              <div className="table__cell table__cell--yellow">
                €3,469.21
              </div>

            </div>

            <div className="table__row">
              <div className="table__cell">
                <i className="ico-ribbon-light">
                  <span>3</span>
                </i>
              </div>

              <div className="table__cell">7843iuf</div>

              <div className="table__cell table__cell--yellow">
                €3,469.21
              </div>

            </div>

            <div className="table__row">
              <div className="table__cell">
                <i className="ico-ribbon">
                  <span>4</span>
                </i>
              </div>

              <div className="table__cell">7843iuf</div>

              <div className="table__cell table__cell--yellow">
                €3,469.21
              </div>

            </div>

            <div className="table__row">
              <div className="table__cell">
                <i className="ico-ribbon">
                  <span>5</span>
                </i>
              </div>

              <div className="table__cell">7843iuf</div>

              <div className="table__cell table__cell--yellow">
                €3,469.21
              </div>

            </div>

          </div>

        </div>
      </div>

    );
  }
}

export default SectionHall;
