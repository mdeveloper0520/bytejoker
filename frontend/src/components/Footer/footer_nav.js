import React from "react";
import { Link } from 'react-router-dom';

const pages = [{
  pagename: 'ABOUT',
  path: '/about'
  },{
  pagename: 'AFFILIATE',
  path: '/affiliate'
  },{
  pagename: 'STATISTICS',
  path: '/statistics'
  },{
  pagename: 'SUPPORT',
  path: '/support'
}];

class FooterPage extends React.Component{
  render() {
    return (
      <li>
        <Link to={this.props.path} >{this.props.page}</Link>
      </li>
    );
  }
}

class FooterNav extends React.Component {
  render() {
    return (
      <nav className="footer__nav">
        <ul>
          {pages.map((page, i) =>
            <FooterPage key={i} page={page.pagename} path={page.path} />
          )}
        </ul>
      </nav>
    );
  }
}

export default FooterNav;
