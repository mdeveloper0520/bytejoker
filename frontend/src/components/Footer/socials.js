import React from "react";
import { Link } from 'react-router-dom';
import Select2 from 'react-select2-wrapper';

class Socials extends React.Component {
  render() {
    return (
      <div className="socials">
        <ul>
          <li>
            <Link to="/" >
              <i className="fa fa-facebook" />
            </Link>
          </li>
          <li>
            <Link to="/" >
              <i className="fa fa-twitter" />
            </Link>
          </li>
          <li>
            <Link to="/" >
              <i className="fa fa-instagram" />
            </Link>
          </li>
          <li>
            <Link to="/" >
              <i className="fa fa-pinterest-p" />
            </Link>
          </li>
          <li>
            <Link to="/" >
              <i className="fa fa-google-plus" />
            </Link>
          </li>
        </ul>
        <Select2
          name="field-type-currency"
          id="field-type-currency"
          className="select-small"
          data={[
            {text:'€ EUR',id:1},
            {text:'$ USD',id:2}
          ]}
          defaultValue={1}
          options={
            {
              minimumResultsForSearch: Infinity
            }
          }
        />
      </div>
    );
  }
}

export default Socials;
