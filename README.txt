
Install bitcoind 

Run command in python shell 'bitcoind -regtest -daemon'
Run command in python shell 'bitcoind -regtest -daemon'
Run command in python shell 'telnet localhost 18332'

Edit the bitcoin.conf file in the '.bitcoin' folder created in home folder
Add following contents:

--------------------------------------------------------------------------------

regtest=1

rpcuser=root
rpcpassword=toor
listen=1
server=1
dbcache=50
daemon=1
testnet=0
maxuploadtarget=200
server=1

--------------------------------------------------------------------------------

Create a superuser using command 'python manage.py createsuperuser'.
Enter admin email and password.

In localhost:8000/admin, login with created superuser email and password.

Under Tab 'Django Bitcoin'

1. Create a master wallet,

  'label': 'master_wallet'

2. Create a test wallet,


Assign this test wallet to a user.
---------------------------------------------------------------------------------

Deposit url : http://127.0.0.1:8000/api/v1/base/deposit

Here the input parameters are:

address : Address of wallet where bitcoins are deposited.
amount : Amount that should be deposited
label : Label for that address
Message : A description for that transaction


Withdraw url : http://127.0.0.1:8000/api/v1/base/withdraw

Here the input parameters are:

address : Address of wallet from which bitcoin needs to be withdrawn.
amount : Amount that should be withdrawed
label : Label for that address




